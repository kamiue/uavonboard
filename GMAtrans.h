#ifndef __GMATRANS_H__
#define __GMATRANS_H__

#define RADIUS_EARTH_METER (6378.137*1000.0)
#define Dquad2SYSID 138
#define Dquad2COMPID 190
#define UGVSYSID 1
#define UGVCOMPID 1
#define DebugMode 1

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <termios.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <fstream>
#include <stdarg.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>


// Mavlink protocol lib
#define MAVLINK_USE_CONVENIENCE_FUNCTIONS
#include "./mavlink/include/autoquad/mavlink.h"


#pragma warning( disable : 4996 )
#define MAX_NAMELENGTH 256
#define MULTICAST_ADDRESS "239.255.42.99"     		// IANA, local network
#define PORT_DATA 1511                			// Default multicast group


// Socket compatible
#ifndef INVALID_SOCKET
#define INVALID_SOCKET -1
#endif
#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif

// IP and port list
#define EXTERNALGPS_IPADDRESS "192.168.1.74"		// Optitrack broadcasting address

#define QGROUNDCONTROL_IPADDRESS "192.168.1.74"
#define QGROUNDCONTROL_PORT 14550

#define GUMSTIX_PORT_UDP 14551
#define GUMSTIX_PORT_TCP 6668

#define GUMSTIX_IPADDRESS "192.168.1.137"

#define GROUNDROBOT_IPADDRESS "192.178.1.138"
#define GROUNDROBOT_PORT 6667

#define SERIALPORT "/dev/ttyO0"
#define DQUAD_ID 1
#define BUFFER_LENGTH 2041



// UDP Class
class UDPSocket {
public:
    UDPSocket();                            	// Constructor
    virtual ~UDPSocket();                   	// Destructor

    int init(void);
    void set_values(char tmp[128],int,int);

    int udpsendf(uint8_t* buf,uint16_t len);    // Send with format
    ssize_t udpreceive(void* buff);             // Receive data
    void udpclose(void);                        // Finalize

private:
    int sock;                            	// Sockets
    struct sockaddr_in server_addr, client_addr;// Server/Client IP adrress
    char server_ip[128];
    int server_port;
    int client_port;

    ssize_t recsize;
	socklen_t fromlen;
	int bytes_sent;

    int udpopen(void);                		// Open port
};

// TCP Class
class TCPSocket {
public:
    TCPSocket();                            	// Constructor
    virtual ~TCPSocket();                   	// Destructor

    int init_server(void);
    int init_client(void);
    void set_values_server(int);      		//as the server, the value that sock needs
    void set_values_client(char tmp[128],int);

    int tcpsendf_client(uint8_t* buf,uint16_t len);    // Send with format
    ssize_t tcpreceive_server(void* buff);             // Receive data
    void tcpclose(void);                        // Finalize

private:
    int sock_server;                            // Sockets
    int sock_client;

    int sock_server_session;
    struct sockaddr_in session_dest;

    struct sockaddr_in server_addr, client_addr;   // Server/Client IP adrress

    char server_ip[128];
    int server_port;

    ssize_t recsize;
	socklen_t fromlen;
	int bytes_sent;

    int tcpopen_server(void);
    int tcpopen_client(void);                
};



// Serial Class: raw data mode
class SerialRDM {
public:
    SerialRDM(void);                                    // Constructor
    virtual ~SerialRDM();                               // Destructor

    void set_values(char *);

    int init(void);

    void serialsendf(const char* buf,uint16_t len);     // Send with format
    void serialreceive(void* buff,size_t len);          // Receive data
    void serialclose(void);                             // Finalize

    int fd_mavlink;  					//for mavlink to use

private:
    int fd;                                             // port description file
    char *path;                                   	// port directory
    int serialopen(void);                		// Open port
    int serialconfig(void);                             // Config port
};


// GroundRobot class: communicate with groundrobot and get reference points 
class GroundRobot{
public:
    GroundRobot(void);					// Constructor
    virtual ~GroundRobot();				// Destructor

    uint16_t msg_recv_length; 				// Message from groundrobot
    uint8_t msg_recv_buf[BUFFER_LENGTH];
    ssize_t ugv_recsize;

    uint8_t msg_send_buf[BUFFER_LENGTH];		// Message to groundrobot
    uint16_t msg_send_len;

    int init(void);					// Initializer

    float waypt_x;					// Reference points command from groundrobot
    float waypt_y;
    float waypt_z;

private:
    pthread_t recving;
    virtual void loop(void);
    static void *run(void *args) {
        reinterpret_cast<GroundRobot*>(args)->loop();
        return NULL;
    }
};

// QGroundControl class: receive groundstation request and respond with corresponding data
class QGroundControl{
public:
    QGroundControl(void);				// Constructor
    virtual ~QGroundControl();				// Destructor

    uint16_t msg_recv_length;				// Message from groundstation
    uint8_t msg_recv_buf[BUFFER_LENGTH];
    ssize_t qgc_recsize;

    uint8_t msg_send_buf[BUFFER_LENGTH];		// Message to groundstation
    uint16_t msg_send_len;

    int init(void);					// Initializer

private:
    pthread_t recving;
    virtual void loop(void);
    static void *run(void *args) {
        reinterpret_cast<QGroundControl*>(args)->loop();
        return NULL;
    }

};

// Dquad class: communicate with autopilot system 
class Dquad{
public:
    Dquad(void);					// Constructor
    virtual ~Dquad();					// Destructor

    void set_values(int);				// Set values for communciation config

    uint16_t msg_recv_length;				// Message from autopilot board
    uint8_t msg_recv_buf[2014];
    int qgc_bytes_sent;					

    int init(void);
	
    int wayptreachflag;					// Flag for reaching a reference point

private:
    int quadID;						// id for autopilot system
    pthread_t recving;
    virtual void loop(void);
    static void *run(void *args) {
        reinterpret_cast<Dquad*>(args)->loop();
        return NULL;
    }
};


// eCAM class: use camera to track the position of a ball onground
class eCAM {
public:
    eCAM(void);						// Constructor
    virtual ~eCAM();					// Destructor

    void set_values(int para1,int para2);		// Config for image processing

    double cirlce_x,circle_y;				// Center position of the circle
    int init(void);

private:
    int hough_para1;
    int hough_para2;
    CvCapture *capture=0;
    pthread_t tracking;
    virtual void loop(void);
    static void *run(void *args) {
        reinterpret_cast<eCAM*>(args)->loop();
        return NULL;
    }   
};

// ExternalGPS class: receive indoor localization signal
class ExternalGPS {
public:
    ExternalGPS(void);					// Constructor
    virtual ~ExternalGPS();				// Destructor

    void set_values(int id, char myip[100], char serverip[100]);

    double Lat_mav,Lon_mav,Alt_mav;			// UAV position signals
    float Yaw_mav;

    int init(void);

private:
    int quadID;
    char szMyIPAddress[128];
    char szServerIPAddress[128];
    float x_n,y_n,z_n,yaw_n,x_ref,y_ref,z_ref,yaw_ref,pitch,roll,gaz,yaw;
    float q_x,q_y,q_z,q_w;
    int DataSocket;

    pthread_t recving;
    virtual void loop(void);
    static void *run(void *args) {
        reinterpret_cast<ExternalGPS*>(args)->loop();
        return NULL;
    }

    char  szData[20000];
    int addr_len;
    struct sockaddr_in TheirAddress;

    // Analyse the data
    void Unpack(char*,int);
    void RadiansToDegrees(float *value);
    void GetEulers(float qx, float qy, float qz, float qw, float *angle1,float *angle2, float *angle3);
};

#endif
