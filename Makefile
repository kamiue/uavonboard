COMPILER = g++
TARGET=test
OPTIONS = -I/usr/include/opencv -I/usr/include  -L/usr/lib -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_flann -lpthread -lrt
MAVLINK_HEADER=/mavlink/include
TEST_CC = main.cc udp.cc tcp.cc ExternalGPS.cc dquad.cc SerialRDM.cc qgroundcontrol.cc groundrobot.cc eCAM.cc
TEST_OBJ=${TEST_CC:.cc=.o}
${TARGET}:${TEST_OBJ}
	${COMPILER} ${TEST_OBJ} GMAtrans.h -I ${MAVLINK_HEADER}  -o ${TARGET} $(OPTIONS) 
clean:
	@rm -f *.o *~${TARGET} ${TARGET}.exe gma_test
