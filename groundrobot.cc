#include "GMAtrans.h"
#include "GMAtrans.h"
extern SerialRDM serial;
extern TCPSocket dquad_server;
extern Dquad dquad;
//extern pthread_mutex_t mutex_reachwaypt;

GroundRobot::GroundRobot(void)
{

}

GroundRobot::~GroundRobot()
{

}

int GroundRobot::init()
{
    pthread_create(&recving,NULL,run,this);
    return 1;
}

void GroundRobot::loop(void)
{
   int i=0;
   int send_counter = 0;
   int receive_counter = 0;
   unsigned int temp = 0;
   // dquad.wayptreachflag = 1;
    while(1)
    {
        memset(msg_recv_buf, 0, BUFFER_LENGTH );
	ugv_recsize = dquad_server.tcpreceive_server(msg_recv_buf);
	if (ugv_recsize > 0)
      	{
            send_counter++;
	    mavlink_message_t msg,msgToquad;
	    mavlink_status_t status;
	    printf("receive waypt = %d\n",receive_counter++);

	    for (i = 0; i < ugv_recsize; ++i)
	    {
		if (mavlink_parse_char(MAVLINK_COMM_0, msg_recv_buf[i], &msg, &status))
		{
		    waypt_x = mavlink_msg_aq_robotmsg_get_x(&msg);
		    waypt_y = mavlink_msg_aq_robotmsg_get_y(&msg);
		    waypt_z = mavlink_msg_aq_robotmsg_get_z(&msg);

		    uint8_t buf[2014];
                    uint8_t frame = MAV_FRAME_GLOBAL_RELATIVE_ALT;
                    uint16_t command = MAV_CMD_NAV_WAYPOINT;
                    uint8_t current=0;
                    uint8_t autocontinue=0;
                    float param1=0.5;//radius in meters
                    float param2=0.0;//dwell time in miliseconds
                    float param3=0.7;//max horizontal speed m/s
                    float param4=0.0;
                    float y = waypt_x/RADIUS_EARTH_METER*180.0f/3.14159265f;
                    float x =-waypt_z/RADIUS_EARTH_METER*180.0f/3.14159265f;
                    float z =0.0; // relative altitude

                    
		    if ((dquad.wayptreachflag == 1)||(send_counter > 10))
		    {
	                mavlink_message_t msgToquad;
                        mavlink_msg_mission_item_pack(UGVSYSID, UGVCOMPID, &msgToquad,
                                            Dquad2SYSID, Dquad2COMPID, 0, frame, command, current,autocontinue, param1, param2, param3, 				param4, x, y, z);
                        int len = mavlink_msg_to_send_buffer(buf, &msgToquad);
                        serial.serialsendf((const char*)buf,len);
                        memset(&buf,0,2014);
                        dquad.wayptreachflag = 0;
                        send_counter = 0;
                    }


	 	    // Packet received
		    //printf("x=%f\n",waypt_x);
		    //printf("z=%f\n",waypt_z);

                    //uint8_t buf[2014];
/*
	    	    if (dquad.wayptreachflag == 1)
		    {
		        pthread_mutex_lock(&mutex_reachwaypt);
	    	        uint8_t buf[2014];
            	        int count=1;
		        //printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
            	        mavlink_msg_mission_count_pack(UGVSYSID, UGVCOMPID,&msgToquad,
					       Dquad2SYSID, Dquad2COMPID, count);
              	        int len = mavlink_msg_to_send_buffer(buf, &msgToquad);
            	        serial.serialsendf((const char*)buf,len);
		        //printf("Length is %d\n",len);
		        dquad.wayptreachflag = 0;
		        pthread_mutex_unlock(&mutex_reachwaypt);
		    }
*/
		}
      	    }
	}
	memset(msg_recv_buf, 0, BUFFER_LENGTH);
    }
}
