#include "GMAtrans.h"

TCPSocket::TCPSocket()
{

}

TCPSocket::~TCPSocket()
{

}

void TCPSocket::set_values_client(char serverip[128],int serverport)
{
    strcpy(server_ip,serverip);
    server_port = serverport;
}

void TCPSocket::set_values_server(int serverport)
{
    server_port = serverport;
}

int TCPSocket::init_server()
{
     tcpopen_server();
     return 1;
}

int TCPSocket::init_client()
{
     tcpopen_client();
     return 1;
}

int TCPSocket::tcpopen_server()
{
//int host_port= 6667;

 //   struct sockaddr_in my_addr;

 //   int hsock;
    int * p_int;


    socklen_t addr_size = 0;

  //  int* csock;
   // sockaddr_in sadr;



    sock_server = socket(AF_INET, SOCK_STREAM, 0);
    if(sock_server == -1){
        printf("ServerThread: Error initializing socket %d\n", errno);
        close(sock_server);
		exit(EXIT_FAILURE);
    }

    p_int = (int*)malloc(sizeof(int));
    *p_int = 1;

    if( (setsockopt(sock_server, SOL_SOCKET, SO_REUSEADDR, (char*)p_int, sizeof(int)) == -1 )||
       (setsockopt(sock_server, SOL_SOCKET, SO_KEEPALIVE, (char*)p_int, sizeof(int)) == -1 ) ){
        printf("ServerThread: Error setting options %d\n", errno);
        free(p_int);
         close(sock_server);
		exit(EXIT_FAILURE);
    }
    free(p_int);

    server_addr.sin_family = AF_INET ;
    server_addr.sin_port = htons(server_port);

    memset(&(server_addr.sin_zero), 0, 8);
    server_addr.sin_addr.s_addr = INADDR_ANY ;

    if( bind(sock_server, (sockaddr*)&server_addr, sizeof(server_addr)) == -1 ){
        fprintf(stderr,"ServerThread: Error binding to socket, make sure nothing else is listening on this port %d\n",errno);
        close(sock_server);
		exit(EXIT_FAILURE);
    }
    if(listen(sock_server, 10) == -1 ){
        fprintf(stderr, "ServerThread: Error listening %d\n",errno);
        close(sock_server);
		exit(EXIT_FAILURE);
    }

    //Now lets do the server stuff

    addr_size = sizeof(sockaddr_in);


    printf("ServerThread: waiting for a connection\n");
  //  csock = (int*)malloc(sizeof(int));
    if((sock_server_session = accept(sock_server, (sockaddr*)&session_dest, &addr_size))!= -1){


        printf("---------------------\nReceived connection from %s\n",inet_ntoa(session_dest.sin_addr));
        //pthread_create(&thread_id,0,&SocketHandler, (void*)csock );

    }
    else{
        fprintf(stderr, "ServerThread: Error accepting %d\n", errno);
    }

    return 1;
}

int TCPSocket::tcpopen_client()
{
   // struct sockaddr_in my_addr;

   // int hsock;
    int * p_int;
    int err;

    sock_client = socket(AF_INET, SOCK_STREAM, 0);
    if(sock_client == -1){
        printf("SockInit: Error initializing socket %d\n",errno);
        close(sock_client);
		exit(EXIT_FAILURE);
        //goto FINISH;
    }

    p_int = (int*)malloc(sizeof(int));
    *p_int = 1;

    if( (setsockopt(sock_client, SOL_SOCKET, SO_REUSEADDR, (char*)p_int, sizeof(int)) == -1 )||
       (setsockopt(sock_client, SOL_SOCKET, SO_KEEPALIVE, (char*)p_int, sizeof(int)) == -1 ) ){
        printf("SockInit: Error setting options %d\n",errno);
     //   free(p_int);
        close(sock_client);
		exit(EXIT_FAILURE);
        //goto FINISH;
    }
    free(p_int);

    client_addr.sin_family = AF_INET ;
    client_addr.sin_port = htons(server_port);

    memset(&(client_addr.sin_zero), 0, 8);
    client_addr.sin_addr.s_addr = inet_addr(server_ip);

    if( connect( sock_client, (struct sockaddr*)&client_addr, sizeof(client_addr)) == -1 ){
        if((err = errno) != EINPROGRESS){
            fprintf(stderr, "SockInit: Error connecting socket %d\n", errno);
            close(sock_client);
		exit(EXIT_FAILURE);
            //goto FINISH;
        }
    }
    return 1;

}

ssize_t TCPSocket::tcpreceive_server(void *buff)
{
    ssize_t recsize_tmp;
    recsize_tmp = recv(sock_server_session, (void *)buff, BUFFER_LENGTH, 0);
    return(recsize_tmp);
}

int TCPSocket::tcpsendf_client(uint8_t* buf,uint16_t len)
{
    bytes_sent = send(sock_client, buf, len, 0);

    return (bytes_sent);
}


