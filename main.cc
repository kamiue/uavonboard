
#include "GMAtrans.h"

using namespace std;

// Comunication protocal
SerialRDM serial;
UDPSocket qgcsock;
TCPSocket dquad_client;
TCPSocket dquad_server;

// Optitrack system
ExternalGPS externalgps;

// Onboard camera
eCAM ecam;

// Groundstation
QGroundControl qgc;

// Autopilot board
Dquad dquad;

// Groundrobot
GroundRobot groundrobot;

//pthread_mutex_t mutex_reachwaypt; // used to protect data transmission through serial port

int main(int argc, char* argv[])
{
    //  pthread_mutex_init(&mutex_reachwaypt,NULL); 
    
    // Initialize serial port
    serial.set_values(SERIALPORT);
    if(serial.init() != 1){
        printf("SerialComm error: fail to initialize\n");
        exit(-1);
    }

    // Initialize udp sock for qgc
    qgcsock.set_values(QGROUNDCONTROL_IPADDRESS,QGROUNDCONTROL_PORT,GUMSTIX_PORT_UDP);
    if(qgcsock.init() != 1){
        printf("qgcSocket error: fail to initialize\n");
        exit(-1);
    }

    // Initialize gumstix on dquad as client to join groundrobot sock and send msg to groundrobot
/*  dquad_client.set_values_client("127.0.0.1",6667);
    if(dquad_client.init_client() != 1){
        printf("dquadSocket_Client error: fail to initialize\n");
        exit(-1);
    }
*/

    // Initialize Optitrack system to get localization data and pack the data in mavlink_externalgps mgs, then send them to serial port
    externalgps.set_values(DQUAD_ID,GUMSTIX_IPADDRESS,EXTERNALGPS_IPADDRESS);
    if(externalgps.init() != 1){
        printf("ExternalGPS error: fail to initialize\n");
        exit(-1);
    }

    ecam.set_values(20,50);
    if(ecam.init() != 1){
        printf("eCAM error: fail to initialize\n");
        exit(-1);
    }

    // Initialize qgroundcontrol thread to get mavlink msg from qgc and send it to serial port
    if(qgc.init() != 1){
        printf("QGroundControl error: fail to initialize\n");
        exit(-1);
    }


    // Initialize dquad thread to decode msg from serial port and send the msg out to different destinations (qgc, groundrobot, etc.)
    dquad.set_values(DQUAD_ID);
    if(dquad.init() != 1){
        printf("Dquad error: fail to initialize\n");
        exit(-1);
    }

    // Initialize gumstix on dquad as server to listen to connection request from g$
/*  dquad_server.set_values_server(GROUNDROBOT_PORT);
    if(dquad_server.init_server() != 1){
        printf("dquadSocket_Server error: fail to initialize\n");
        exit(-1);
    }

    // Initialize groundrobot thread to handle messages from groundrobot sending by$
    if(groundrobot.init() != 1){
        printf("GroundRobot error: fail to initialize\n");
        exit(-1);
    }
*/
    while(1);
    
    return 0;
 	
}



