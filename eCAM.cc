#include "GMAtrans.h"
using namespace cv;
extern ExternalGPS externalgps;
extern SerialRDM serial;
extern Dquad dquad;
eCAM::eCAM(void)
{

}

eCAM::~eCAM()
{

}

void eCAM::set_values(int para1,int para2)
{
  hough_para1=para1;
  hough_para2=para2;
}


// Success:1
int eCAM::init(void)
{   
   capture=cvCaptureFromCAM(0);
   if(!capture){
       printf("eCAM:Failed to connect to the camera.\n");
   }
   pthread_create(&tracking,NULL,run,this);
   return 1;
}


void eCAM::loop(void)
{
   Mat frame;
   Mat src_gray;
   int center_x,center_y;
   int setpoint_x=320;
   int setpoint_y=240;
   int dx,dy;
   double Dx,Dy;
   double R = 0.4;
   double waypt_lat;
   double waypt_lon;
   vector<Vec3f> circles;
   int i=0;
   int send_counter = 0;
   unsigned int temp = 0;
   while(1){
        frame=cvQueryFrame(capture);       
        src_gray=frame;
        GaussianBlur(src_gray,src_gray,Size(9,9),2,2);
        HoughCircles(src_gray,circles,CV_HOUGH_GRADIENT,1,src_gray.rows/8,hough_para1,hough_para2,0,0);
        if(circles.size()==1)
	{
	  Point center(cvRound(circles[0][0]),cvRound(circles[0][1]));
	  int radius=cvRound(circles[0][2]);
	  circle(frame,center,3,Scalar(0,255,0),-1,8,0);
	  circle(frame,center,radius,Scalar(0,0,255),3,8,0);
	  printf("Center position of cirle is %d,%d\n",cvRound(circles[0][0]),cvRound(circles[0][1]));
	
	  center_x=cvRound(circles[0][0]);
	  center_y=cvRound(circles[0][1]);
	  dx=-(setpoint_x-center_x);
	  dy=setpoint_y-center_y;
    	  Dx = cos(atan2(dy,dx))*R;
	  Dy = sin(atan2(dy,dx))*R;
          
	  waypt_lat=externalgps.Lat_mav+Dy*R/RADIUS_EARTH_METER*180.0f/3.14159265f;
          waypt_lon=externalgps.Lon_mav+Dx*R/RADIUS_EARTH_METER*180.0f/3.14159265f;
	
         
          send_counter++;
          uint8_t buf[2014];
          uint8_t frame = MAV_FRAME_GLOBAL_RELATIVE_ALT;
          uint16_t command = MAV_CMD_NAV_WAYPOINT;
          uint8_t current=0;
          uint8_t autocontinue=0;
    	  float param1=0.3;//radius in meters
    	  float param2=0.0;//dwell time in miliseconds
    	  float param3=0.7;//max horizontal speed m/s
    	  float param4=0.0;
    	  float y = waypt_lat;
      	  float x = waypt_lon;
    	  float z =0.0; // relative altitude

    	  //for reference only
  	  //Lat_mav = x/RADIUS_EARTH_METER*180.0f/3.14159265f;
   	  //Lon_mav = -z/RADIUS_EARTH_METER*180.0f/3.14159265f;
	    if ((dquad.wayptreachflag == 1)||(send_counter > 10))
	    {

	      mavlink_message_t msgToquad;
              mavlink_msg_mission_item_pack(UGVSYSID, UGVCOMPID, &msgToquad,
			Dquad2SYSID, Dquad2COMPID, 0, frame, command, current,autocontinue, param1, param2, param3, param4, x, 				y, z);
	      int len = mavlink_msg_to_send_buffer(buf, &msgToquad);
	      serial.serialsendf((const char*)buf,len);
              memset(&buf,0,2014);
	      dquad.wayptreachflag = 0;
              send_counter = 0;
	     }
        }
	else if (circles.size()==0) 
	{
	  printf("eCAM warning: no circle\n");
	}
	else
	{
	  printf("eCAM warning: more than one circle\n");	
	}   
      
   }
}
