
#include "GMAtrans.h"

UDPSocket::UDPSocket()
{

}


UDPSocket::~UDPSocket()
{

}

void UDPSocket::set_values(char serverip[128],int serverport,int clientport)
{
    strcpy(server_ip,serverip);

    server_port = serverport;
    client_port = clientport;
}

int UDPSocket::init()
{
    udpopen();

    return 1;
}

int UDPSocket::udpopen()
{
    sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

    memset(&client_addr, 0, sizeof(client_addr));
	client_addr.sin_family = AF_INET;
	client_addr.sin_addr.s_addr = INADDR_ANY;
	client_addr.sin_port = htons(client_port);

	/* Bind the socket to port 14551 - necessary to receive packets from qgroundcontrol */
	if (-1 == bind(sock,(struct sockaddr *)&client_addr, sizeof(struct sockaddr)))
    {
		perror("error bind failed");
		close(sock);
		exit(EXIT_FAILURE);
    }

	/* Attempt to make it non blocking */
	if (fcntl(sock, F_SETFL, O_NONBLOCK | FASYNC) < 0)
    {
		fprintf(stderr, "error setting nonblocking: %s\n", strerror(errno));
		close(sock);
		exit(EXIT_FAILURE);
    }


	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(server_ip);
	server_addr.sin_port = htons(server_port);

    return 1;
//printf("check\n");

}

int UDPSocket::udpsendf(uint8_t* buf,uint16_t len)
{
    bytes_sent = sendto(sock, buf, len, 0, (struct sockaddr*)&server_addr, sizeof(struct sockaddr_in));

    return (bytes_sent);

}


ssize_t UDPSocket::udpreceive(void* buf)
{
    recsize = recvfrom(sock, (void *)buf, BUFFER_LENGTH, 0, (struct sockaddr *)&server_addr, &fromlen);
    return(recsize);
}


void UDPSocket::udpclose(void)
{
    close(sock);
}

