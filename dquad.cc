#include "GMAtrans.h"

extern SerialRDM serial;
extern UDPSocket qgcsock;
extern TCPSocket dquad_client;
extern GroundRobot groundrobot;
//extern pthread_mutex_t mutex_reachwaypt;

Dquad::Dquad(void)
{

}

Dquad::~Dquad()
{

}

void Dquad::set_values(int id)
{
    quadID = id;
    wayptreachflag = 1;
}

int Dquad::init(void)
{
    pthread_create(&recving,NULL,run,this);
    return 1;
}

void Dquad::loop(void)
{
	char buf;
int reachidtest=0;

    while(1)
    {
        mavlink_message_t msg;
        mavlink_status_t status;

        serial.serialreceive(&buf,1);

        if (mavlink_parse_char(MAVLINK_COMM_0, buf, &msg, &status))
        {
                // Packet received from groundrobot
/*              if (msg.msgid==MAVLINK_MSG_ID_MISSION_REQUEST)
                {
                    printf("mission request\n");
                    uint8_t buf[2014];
                    uint8_t frame = MAV_FRAME_GLOBAL_RELATIVE_ALT;
                    uint16_t command = MAV_CMD_NAV_WAYPOINT;
                    uint8_t current=0;
                    uint8_t autocontinue=0;
                    float param1=0.5;//radius in meters
                    float param2=0.0;//dwell time in miliseconds
                    float param3=0.7;//max horizontal speed m/s
                    float param4=0.0;
                    float y = groundrobot.waypt_x/RADIUS_EARTH_METER*180.0f/3.14159265f;
                    float x =-groundrobot.waypt_z/RADIUS_EARTH_METER*180.0f/3.14159265f;
                    float z =0.0; // relative altitude

                    //for reference only
                    //Lat_mav = x/RADIUS_EARTH_METER*180.0f/3.14159265f;
                    //Lon_mav = -z/RADIUS_EARTH_METER*180.0f/3.14159265f;

                    mavlink_message_t msgToquad;
                    mavlink_msg_mission_item_pack(UGVSYSID, UGVCOMPID, &msgToquad,
                                       Dquad2SYSID, Dquad2COMPID, 0, frame, command, current,autocontinue, param1, param2, param3, param4, x, 					       y, z);
                    int len = mavlink_msg_to_send_buffer(buf, &msgToquad);
                    serial.serialsendf((const char*)buf,len);
                    memset(&buf,0,2014);
		 }
*/
//              if (msg.msgid==MAVLINK_MSG_ID_MISSION_ACK)
//                        printf("ACK received\n");
		if (msg.msgid==MAVLINK_MSG_ID_MISSION_ITEM_REACHED)
                {

			printf("Waypoint reached = %d\n",reachidtest++);
/*    			mavlink_message_t msgToquad_SetCurrent;
			uint8_t buf_SetCurrent[2014];
			mavlink_msg_mission_set_current_pack(UGVSYSID, UGVCOMPID,&msgToquad_SetCurrent, Dquad2SYSID, Dquad2COMPID,1);
			int len_SetCurrent = mavlink_msg_to_send_buffer(buf_SetCurrent, &msgToquad_SetCurrent);
                        serial.serialsendf((const char*)buf_SetCurrent,len_SetCurrent);
			memset(&buf_SetCurrent,0,2014);
*/
			wayptreachflag = 1;
		
		}        
//              printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
                msg_recv_length = mavlink_msg_to_send_buffer(msg_recv_buf, &msg);
                qgc_bytes_sent = qgcsock.udpsendf(msg_recv_buf,msg_recv_length);
                memset(msg_recv_buf, 0, msg_recv_length);
            }
        memset(&buf, 0, 1);
    }
}
